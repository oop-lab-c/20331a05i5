 #include <iostream>
using namespace std;
class A {
    public: void showA (){
        cout <<" class A called\n";
        
    }
    
};
class B: public A {
    public:
    void showB(){
        cout<<"class B called\n";
        
    }
    
};
class C : public A{
    public:
    void showC(){
        cout<<"class C called\n";
        
    }
   
    
};
class D : public B, public C{
    void showD(){}
};

int main ()
{
    D obj;
    obj.showB();
    obj.showC() ;
    return 0;
    
}
