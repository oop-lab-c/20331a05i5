class Parent 
{
  
  public void show()
  {
    System.out.println("Parent inheriting properties to child\n");
  }
}
class Child extends Parent 
{
  public void show1()
  {
    System.out.println("I am a child class who belongs to parent class\n");
  }
  public static void main(String args[])
  {
    Child obj = new Child ();
    obj.show();
    obj.show1();
    
  }
}


