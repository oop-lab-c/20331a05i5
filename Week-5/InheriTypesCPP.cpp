#include <iostream>
using namespace std;
class A{
    public:
    void MyFunc(){
        cout<<" Base\n";
    }
};
class B : public A{
    public:
    void MyFunc1 () {
        cout <<" simple\n";
    }
    
};
class C : public B {
    public:
    void MyFunc2() {
        cout<<"multilevel\n";
        
    }
    
};

    

class E: public A {
    public:
    void MyFunc4(){
        cout <<"heir1\n";
    }
    
};
class F: public A{
    public:
    void MyFunc5(){
        cout <<" Heir 2\n";
    }
    
};
class D : public E,F{
};
int main(){
    A obj;
    B obj1;
    C obj2;
    D obj5;
    E obj3;
    F obj4;
    obj1.MyFunc();
    obj1.MyFunc1();
    obj2.MyFunc2();
    obj2.MyFunc1();
    obj3.MyFunc();
    obj4.MyFunc();
    obj5.MyFunc4();
    return 0;
}

