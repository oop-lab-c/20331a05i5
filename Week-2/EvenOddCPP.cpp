#include <iostream>
using namespace std;
void evenodd(int num){
    if ( num % 2 == 0)
    cout << num << " is an even.";
  else
    cout << num << " is an odd.";
};

int main() {
  int num;

  cout << "Enter a Number: ";
  cin >> num;
  evenodd(num);
  return 0;
}
