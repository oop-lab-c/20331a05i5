class Main
{
    public void student()
    {
        String name = "Tarun";
        double sempercentage = 88.98;
        int rollno = 56;
        String collegename = "MVGR";
        int collegecode = 33;
        System.out.println("Name : "+name);
        System.out.println("SemPercentage : "+sempercentage);
        System.out.println("RollNo : "+rollno);
        System.out.println("CollegeName : "+collegename);
        System.out.println("CollegeCode : "+collegecode);
    }
    protected void finalize()
    {
        System.out.println("Finalize method Invoked");
    }
    public static void main (String[] args) {
        Main obj = new Main();
        obj.student();
        obj.finalize();
    }
}