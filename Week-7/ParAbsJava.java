
import java.util.Scanner;
abstract class impureAbstraction{
    abstract void display();{
}
}
class A extends impureAbstraction
{
    void display()
    {
        System.out.println("impure abstarction is achieved by abstract classes.");
    }
    public static void main(String[] args)
    {
        A obj = new A();
        obj.display();
    }
}