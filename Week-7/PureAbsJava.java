import java.util.Scanner;
interface pureAbstraction
{
    void display();
}
class A implements pureAbstraction
{
    public void display()
    {
        System.out.println("pure abstarction is achieved by interfaces only.");
    }
    public static void main(String[] args)
    {
        A obj = new A();
        obj.display();
    }
}