import jav.util.Scanner;
class Base
{
	public int func(int i)
	{
		System.out.print("int: ");
		return i+3;
	}
}
class Derived extends Base
{
	public double func(double i)
	{
		System.out.print("double : ");
		return i + 3.3;
	}
}
class INoverl
{
	public static void main(String args[])
	{
		Derived obj = new Derived();
		System.out.println(obj.func(3));
		System.out.println(obj.func(3.3));
	}
}