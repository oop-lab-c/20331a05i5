import java.util.Scanner;
class EvenOddJava
{
    void EvenOdd(int a) {
        if (a % 2 == 0)
            System.out.println("Number is even");
        else
            System.out.println("Number is odd");
    }
    public static void main(String[] args){
        System.out.println("Enter an integer:");
        Scanner input = new Scanner(System.in);
        int a = input.nextInt();
        EvenOddJava obj = new EvenOddJava();
        obj.EvenOdd(a);
    }
}